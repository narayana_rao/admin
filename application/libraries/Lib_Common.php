<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Lib_Common
{
	/**
	 * Constructor
	 */
	function __construct()
	{
		// CI
		$this->ci = &get_instance();

		// CI
		$this->ci = &get_instance();

		// Load configs
		$arrConfigs = isset($this->arrConfigs) ? $this->arrConfigs : null;
		if($arrConfigs != null)
			foreach ($arrConfigs as $strConfig)
				$this->ci->load->config($strConfig);

		// Load models
		$arrModels = isset($this->arrModels) ? $this->arrModels : null;
		if($arrModels != null)
			foreach ($arrModels as $strModel)
				$this->ci->load->model($strModel);

		// Get the language
		$language = $this->ci->config->item('rest_language');
		if ($language === null)
			$language = 'english';

		// Load languages
		$arrLanguageAPIs = isset($this->arrLanguageAPIs) ? $this->arrLanguageAPIs : null;
		if($arrLanguageAPIs != null)
			foreach ($arrLanguageAPIs as $arrLanguageAPI)
				$this->ci->lang->load($arrLanguageAPI, $language);
	}
}
