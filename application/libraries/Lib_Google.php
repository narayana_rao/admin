<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lib_Google extends Lib_Common
{

	/**
	 * __construct method
	 */
	public function __construct()
	{
		parent::__construct();

	}

	/**
	 * @param string[] $p_strAddress
	 * @return string[]
	 */
	public function getLatLangByAddress($p_strAddress)
	{
		// GoggleGeocode
		$strGoogleGeoCodeKey = $this->ci->config->item('google_geoocode_key');
		$strUrl = "https://maps.googleapis.com/maps/api/geocode/json?address=".urlencode($p_strAddress)."&key=".$strGoogleGeoCodeKey;

		$JsonLocationData = file_get_contents($strUrl);

		// decode the json
		$arrLocationData = json_decode($JsonLocationData, true);

		return $arrLocationData;
	}
}
