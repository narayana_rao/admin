<?php

class Lib_item extends Lib_Common
{

	public $arrModels = ['items'];

	/**
	 * @param string[] $arrParams
	 * @return string[]
	 */
	public function index()
	{
		$arrData = $this->ci->items->getData();

		$arrResult =  array(
			"success" => "200",
			"response" =>$arrData
		);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}
}
