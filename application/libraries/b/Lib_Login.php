<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lib_Login extends Lib_Common
{

	public $arrModels = [array('auth')];


	/**
	 * __construct method
	 */
	public function __construct()
	{
		parent::__construct();

		// Load Lib_Common Library
		$this->ci->load->library('Lib_Google');
	}

	/**
	 * @param string[] $p_arrParam
	 * @return string[]
	 */
	public function insertData($p_arrParam)
	{
		// Check Phone Number
		$arrData = $this->ci->auth->checkPhoneNumberExist($p_arrParam);

		if($arrData)
		{
			if($arrData->status == 1)
			{
				$arrResult =  array(
					"success" => "200",
					"response" => false,
					"message" =>"User Register Already",
					"status" =>  $arrData->status
				);
			}
			else
			{
				$arrResult =  array(
					"success" => "200",
					"response" => true,
					"message" =>"Post Done Successfully",
					"id" =>  $arrData->id,
					"otp" => $arrData->otp
				);
			}


			return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
		}

		$p_arrParam['otp'] = $nOtp =  $this->generateNumericOTP(4);

		$nID = $this->ci->auth->postData($p_arrParam);

		$arrResult =  array(
			"success" => "200",
			"response" => "true",
			"message" =>"Post Done Successfully",
			"id" =>  $nID,
			"otp" => $nOtp
		);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}

	/**
	 * @param string[] $p_arrParam
	 * @return string[]
	 */
	public function checkOTP($p_arrParam)
	{
		$arrData = $this->ci->auth->checkOTP($p_arrParam);

		if($arrData)
		{
			// Update Status
			$arrUpdateData =  array(
				'status' => 1,
				'otp' => ''
			);

			$this->ci->auth->updateData($arrUpdateData , $arrData->id);

			$arrResult =  array(
				"success" => "200",
				"response" => true,
				"message" =>"OTP Verified SuccessFully",
				"id" =>  $arrData->id
			);
		}
		else
		{
			$arrResult =  array(
				"success" => "200",
				"response" => false,
				"message" =>"OTP Verified Failed"
			);
		}

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param string[] $p_arrParam
	 * @param int $nID
	 * @return string[]
	 */
	public function insertPassword($p_arrParam, $nID)
	{
		$arUpdateData  = array(
			'password'  => md5($p_arrParam['password']),
			'status'  => 2
		);

		$nUpdateData = $this->ci->auth->updateData($arUpdateData, $nID);

		if($nUpdateData)
			$arrResult =  array(
				"success" => "200",
				"response" => false,
				"message" =>"Registration Done Successfully"
			);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param string[] $p_arrParam
	 * @param int $nID
	 * @return string[]
	 */
	public function updateProfile($p_arrParam, $nID)
	{
		// Set
		$arrUpdateData = $p_arrParam;

		$arrLatLangData = $this->ci->lib_google->getLatLangByAddress($p_arrParam['address'])['results'][0];

		// profile Data
		$arrupdateProfileData = array_merge($arrUpdateData, $arrLatLangData['geometry']['location']);

		// unset
		unset($arrupdateProfileData['categories']);

		$arrUserCategories = explode(',' ,$p_arrParam['categories']);

		foreach ($arrUserCategories as $arrUserCategory)
		{
			$arrInsertCategories[] =  array(
				'user_id' => $nID,
				'category_id' => $arrUserCategory,
			);
		}

		// Delete Catgories
		 $this->ci->auth->delete($nID);

		// Insert User Catgories
		$arrInsertData = $this->ci->auth->postMultiData($arrInsertCategories);

		// Update Profile
		$nUpdateData = $this->ci->auth->updateData($arrupdateProfileData, $nID);

		if($nUpdateData)
			$arrResult =  array(
				"success" => "200",
				"response" => true,
				"message" =>"Profile Udated Successfully"
			);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param string[] $p_arrParam
	 * @return string[]
	 */
	function forgot_password($p_arrParam) {

		// Check Phone Number
		$arrData = $this->ci->auth->checkPhoneNumberExist($p_arrParam);

		if($arrData)
		{
			$p_arrParam['otp'] = $nOtp =  $this->generateNumericOTP(4);

			$arrUpdateData =  array(
				'otp' =>  $nOtp
			);

			$arrResult = $this->ci->auth->updateData($arrUpdateData, $arrData->id);

			$arrResult = array(
				"success" => "200",
				"response" => true,
				"message" => "User Register Already",
				"id" => $arrData->id,
				"otp" => $nOtp
			);

		}
		else
		{
			$arrResult = array(
				"success" => "200",
				"response" => false,
				"message" => "User Not Exit"
			);
		}

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param string[] $p_arrParam
	 * @param int $nID
	 * @return string[]
	 */
	function change_password($p_arrParam, $nID) {

		// Check Phone Number
		$arrData = $this->ci->auth->checkUserByID($nID);

		if($arrData)
		{
			$arUpdateData  = array(
				'password'  => md5($p_arrParam['password']),

			);

			$nUpdateData = $this->ci->auth->updateData($arUpdateData, $nID);

			$arrResult = array(
				"success" => "200",
				"response" => true,
				"message" => "Password Updated Successfully"
			);
		}
		else
		{
			$arrResult = array(
				"success" => "200",
				"response" => false,
				"message" => "User Not Exit"
			);
		}

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param int $nLength
	 * @return string[]
	 */
	function generateNumericOTP($nLength) {

		// all numeric digits
		$strGenerator = "1357902468";

		$result = "";

		for ($nI = 1; $nI <= $nLength; $nI++) {
			$result .= substr($strGenerator, (rand()%(strlen($strGenerator))), 1);
		}

		// Return result
		return $result;
	}
}
