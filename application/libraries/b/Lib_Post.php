<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Lib_Post extends Lib_Common
{

	public $arrModels = [array('post','preference')];


	/**
	 * __construct method
	 */
	public function __construct()
	{
		parent::__construct();

		// Load Lib_Common Library
		$this->ci->load->library('Lib_Google');
	}

	/**
	 * @param string[] $p_arrParam
	 * @return string[]
	 */
	public function insertPostData($p_arrParam)
	{
		$arrLatLangData = $this->ci->lib_google->getLatLangByAddress($p_arrParam['address'])['results'][0];

		$arrPostData = array_merge($p_arrParam, $arrLatLangData['geometry']['location']);

		// Post
		$nID = $this->ci->post->postData($arrPostData);

		if($nID)
			$arrResult =  array(
				"success" => "200",
				"response" => true,
				"message" =>"Post Done Successfully",
				"id" =>  $nID
			);
		else
			$arrResult =  array(
				"success" => "200",
				"response" => false,
				"message" =>"Post Failed",
			);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param string[] $p_arrParam
	 * @param int $nID
	 * @return string[]
	 */
	public function updatePostData($p_arrParam, $nID)
	{
		// Check Phone Number
		$arrData = $this->ci->post->checkPostByID($nID);

		if(empty($arrData))
		{
			$arrResult =  array(
				"success" => "200",
				"response" => false,
				"message" =>"Posts Not Found",
				"id" =>  $nID
			);

			return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
		}

		if(isset($p_arrParam['address']))
		{
			$arrLatLangData = $this->ci->lib_google->getLatLangByAddress($p_arrParam['address'])['results'][0];
			$arrPostData = array_merge($p_arrParam, $arrLatLangData['geometry']['location']);

			// Put
			$nID = $this->ci->post->updateData($arrPostData, $nID);
		}
		else
		{
			// Put
			$nID = $this->ci->post->updateData($p_arrParam, $nID);
		}

		if($nID)
			$arrResult =  array(
				"success" => "200",
				"response" => true,
				"message" =>"Post Updated Successfully",
				"id" =>  $nID
			);
		else
			$arrResult =  array(
				"success" => "200",
				"response" => false,
				"message" =>"Post Updated Failed",
			);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}


	/**
	 * @param string[] $p_arrParam
	 * @return string[]
	 */
	public function insertPreferencesData($p_arrParam)
	{
		// user ID
		$nUserID = $p_arrParam['user_id'];

		// Prefered Categories
		$arrUserCategories = explode(',' ,$p_arrParam['category_ids']);

		foreach ($arrUserCategories as $arrUserCategory)
		{
			$arrPreferedCategories[] =  array(
				'user_id' => $nUserID,
				'category_id' => $arrUserCategory,
			);
		}


		foreach ($p_arrParam['locations'] as $arrUserLocation)
		{
			$arrPreferedLocations[] =  array(
				'user_id' => $nUserID,
				'lat' => $arrUserLocation['lat'],
				'lng' => $arrUserLocation['lng'],
			);
		}

		// Delete preferences
		$this->ci->preference->deletePreferences($nUserID);

		// Insert User Preferred Catgories
		$arrInsertData = $this->ci->preference->postMultiPreferedCategoriesData($arrPreferedCategories);

		// Insert Prefered locations
		$arrInsertData = $this->ci->preference->postMultiPreferedLocationsData($arrPreferedLocations);

		if($arrInsertData)
			$arrResult =  array(
				"success" => "200",
				"response" => true,
				"message" =>"Preferences add Successfully",
			);
		else
			$arrResult =  array(
				"success" => "200",
				"response" => false,
				"message" =>"Preferences add Failed",
			);

		return $this->ci->response($arrResult, REST_Controller::HTTP_OK);
	}
}
