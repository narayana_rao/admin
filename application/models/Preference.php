<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Preference extends Crud
{
    /**
     * __construct method
     */
    public function __construct()
    {
		$this->_user_prefered_locations_table = 'user_prefered_locations';
		$this->_user_prefered_categories_table = 'user_prefered_categories';
    }

    /**
	 * checkPostByID
	 *
	 * @param int  $nID
	 * @return object
	 */
	public function deletePreferences($nID)
	{
		$this->db->where("user_id", $nID);
		return $this->db->delete(array($this->_user_prefered_categories_table, $this->_user_prefered_locations_table));
	}

	/**
	 * postMultiPreferedCategoriesData method
	 */
	public function postMultiPreferedCategoriesData($arrData)
	{
		return $this->db->insert_batch($this->_user_prefered_categories_table, $arrData);
	}

	/**
	 * postMultiPreferedLocationsData method
	 */
	public function postMultiPreferedLocationsData($arrData)
	{
		return $this->db->insert_batch($this->_user_prefered_locations_table, $arrData);
	}



}
