<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends  CI_Model
{
    /**
     * __construct method
     *
     */
    /*public function __construct()
    {
		//$this->_table = 'static_batches';
    }*/

	/**
	 * get method
	 */
	public function getData()
	{
		return $this->db->get($this->_table)->row();
	}


	/**
	 * Post method
	 */
	public function postData($arrData)
	{
		$this->db->insert($this->_table, $arrData);

		return $this->db->insert_id();
	}

	/**
	 * postMultiData method
	 */
	public function postMultiData($arrData)
	{
		return $this->db->insert_batch($this->_table, $arrData);
	}

	/**
	 * update method
	 */
	public function updateData($arrData, $nID)
	{
		$this->db->where('id', $nID);
		$this->db->update($this->_table, $arrData);
		return $nID;
	}

	/**
	 * DeleteData method
	 */
	public function delete($nID)
	{
		$this->db->where('id', $nID);
		return $this->db->delete($this->_table);
	}

}
