<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Crud
{
    /**
     * __construct method
     */
    public function __construct()
    {
		$this->_table = 'users';
		$this->_user_categories = 'user_categories';
    }

	/**
	 * Check OTP
	 *
	 * @param array $arrData
	 * @return object
	 */
    public function checkOTP($arrData)
	{
		$this->db->select("$this->_table.id");
		$this->db->where("phone", $arrData['phone']);
		$this->db->where("otp", $arrData['otp']);
		return $this->db->get($this->_table)->row();
	}


	/**
	 * Check OTP
	 *
	 * @param array $arrData
	 * @return object
	 */
	public function checkPhoneNumberExist($arrData)
	{
		$this->db->select("$this->_table.id,$this->_table.otp, $this->_table.status");
		$this->db->where("phone", $arrData['phone']);
		return $this->db->get($this->_table)->row();
	}


	/**
	 * checkUserByID
	 *
	 * @param array $arrData
	 * @return object
	 */
	public function checkUserByID($nID)
	{
		$this->db->select("$this->_table.id");
		$this->db->where("id", $nID);
		return $this->db->get($this->_table)->row();
	}

	/**
	 * postMultiData method
	 */
	public function postMultiData($arrData)
	{
		return $this->db->insert_batch($this->_user_categories, $arrData);
	}

	/**
	 * DeleteData method
	 */
	public function delete($nID)
	{
		$this->db->where('user_id', $nID);
		return $this->db->delete($this->_user_categories);
	}
}
