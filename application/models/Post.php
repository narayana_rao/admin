<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Post extends Crud
{
    /**
     * __construct method
     */
    public function __construct()
    {
		$this->_table = 'posts';
    }

    /**
	 * checkPostByID
	 *
	 * @param int  $nID
	 * @return object
	 */
	public function checkPostByID($nID)
	{
		$this->db->select("$this->_table.id");
		$this->db->where("id", $nID);
		return $this->db->get($this->_table)->row();
	}
}
