<?php

require APPPATH . 'libraries/REST_Controller.php';

class Posts extends REST_Controller {

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function __construct() {
		parent::__construct();

		// Load Lib_Blogs library
		$this->load->library('b/Lib_Post');
	}

	/**
	 * Post Data
	 *
	 * @uri posts
	 * @httpmethod Posts
	 * @return Response
	 */
	public function index_post()
	{
		$arrPostData = $this->post();

		return  $this->lib_post->insertPostData($arrPostData);
	}

	/**
	 * Update Data
	 *
	 * @uri post/[:id]
	 * @httpmethod Put
	 * @return Response
	 */
	public function index_put($nID)
	{
		$arrPostData = $this->put();

		return  $this->lib_post->updatePostData($arrPostData, $nID);
	}

	/**
	 * Update Data
	 *
	 * @uri preferences
	 * @httpmethod post
	 * @return Response
	 */
	public function preferences_post()
	{
		$arrPostData = $this->post();
		return  $this->lib_post->insertPreferencesData($arrPostData);
	}
}


