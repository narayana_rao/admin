<?php

require APPPATH . 'libraries/REST_Controller.php';

class Reg extends REST_Controller {

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function __construct() {
		parent::__construct();

		// Load Lib_Blogs library
		$this->load->library('b/Lib_login');
	}

	/**
	 * Post Login Data
	 *
	 * @uri login
	 * @httpmethod GET
	 * @return Response
	 */
	public function index_post()
	{
		$arrPostData = $this->post();
		return  $this->lib_login->insertData($arrPostData);
	}

	/**
	 * Get userData
	 *
	 * @uri Reg
	 * @httpmethod GET
	 * @return  Response
	 */
	public function index_get()
	{
		$arrData = $this->get();
		return  $this->lib_login->checkOTP($arrData);
	}

	/**
	 * Generate Password
	 *
	 * @uri reg/password/[:id]
	 * @httpmethod GET
	 * @return  Response
	 */
	public function password_post($nID)
	{
		$arrData = $this->post();
		return  $this->lib_login->insertPassword($arrData, $nID);
	}

	/**
	 * Profile  Update
	 *
	 * @uri reg/profile/[:id]
	 * @httpmethod GET
	 * @return  Response
	 */
	public function profile_put($nID)
	{
		$arrData = $this->put();
		return  $this->lib_login->updateProfile($arrData, $nID);
	}

	/**
	 * Forgot password
	 *
	 * @uri reg/forgot_password
	 * @httpmethod GET
	 * @return  Response
	 */
	public function forgot_password_post()
	{
		$arrData = $this->post();
		return  $this->lib_login->forgot_password($arrData);
	}

	/**
	 * Change password
	 *
	 * @uri reg/change_password/[:id]
	 * @httpmethod GET
	 * @return  Response
	 */
	public function change_password_put($nID)
	{
		$arrData = $this->put();
		return  $this->lib_login->change_password($arrData, $nID);
	}
}


