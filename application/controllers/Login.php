<?php

require APPPATH . 'libraries/REST_Controller.php';

class Login extends REST_Controller {

	/**
	 * Get All Data from this method.
	 *
	 * @return Response
	 */
	public function __construct() {
		parent::__construct();

		// Load Lib_Blogs library
		$this->load->library('b/Lib_login');
	}

	/**
	 * Post Login Data
	 *
	 * @uri Login
	 * @httpmethod GET
	 * @return Response
	 */
	public function index_post()
	{
		$arrPostData = $this->post();
		return  $this->lib_login->insertData($arrPostData);
	}

	/**
	 * Get userData
	 *
	 * @uri login
	 * @httpmethod GET
	 * @return  Response
	 */
	public function index_get()
	{
		$arrData = $this->get();
		return  $this->lib_login->checkOTP($arrData);
	}

	/**
	 * Generate Password
	 *
	 * @uri login/password/[:id]
	 * @httpmethod GET
	 * @return  Response
	 */
	public function password_post($nID)
	{
		$arrData = $this->post();
		return  $this->lib_login->insertPassword($arrData, $nID);
	}

	/**
	 * Profile  Update
	 *
	 * @uri login/profile/[:id]
	 * @httpmethod GET
	 * @return  Response
	 */
	public function profile_put($nID)
	{
		$arrData = $this->put();
		return  $this->lib_login->updateProfile($arrData, $nID);
	}
}


