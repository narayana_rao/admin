<?php
	
if (! defined('BASEPATH')) exit('No direct script access allowed');
 
if (! function_exists('demo')) {
    function demo()
    {
        // get main CodeIgniter object
        $ci = get_instance();
       
        // Write your logic as per requirement
        
    }
}

if (! function_exists('assets')) {
	function assets($strFile, $strFolder = 'css')
	{
		switch ($strFolder) {
			case 'css':
				return base_url('assets/css/'.$strFile);
				break;
			case 'js' :
				return base_url('assets/js/'.$strFile);
				break;
			case 'img' : 
				return base_url('assets/img/'.$strFile);
				break;
			case 'json' : 
				return base_url('assets/json/'.$strFile);
				break;
			default:
				return base_url("assets/$strFolder/$strFile");
				break;
		}
	}
}