<?php

//  all views config data

$config['template'] 			= 'template';
$config['sidebar_view'] 		= 'sidebar';
$config['footer_view']			= 'footer';

$config['welcome_index_view']	= 'welcome';

// Categories
$config['categories_index_view'] = 'categories/index';
